require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

const config = {
  setupFiles: ['dotenv/config'],
  testURL: process.env.BOILERPLATE_PWA_JEST_TEST_URL,
  // TODO: Add more files and directories to test coverage collector
  collectCoverageFrom: ['**/src/{utils,hooks}/*.{js,ts}'],
};

module.exports = config;

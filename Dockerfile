
   
# Install dependencies only when needed
FROM node:14.18.1-alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json package-lock.json ./
COPY .npmrc ./
RUN npm install

# Rebuild the source code only when needed
FROM node:14.18.1-alpine AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .
RUN npm run build

# Production image, copy all the files and run next
FROM node:14.18.1-alpine AS runner
WORKDIR /app

ENV NODE_ENV production


COPY --from=builder /app/build ./build
COPY --from=builder /app/package.json ./package.json
RUN npm install -g serve

EXPOSE 3000

ENV PORT 3000


CMD ["serve", "-s", "build"]
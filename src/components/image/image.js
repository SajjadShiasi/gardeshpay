import React from 'react';
import PropTypes from 'prop-types';

function Image({ alt, endpoint, src, ...restProps }) {
  return <img alt={alt} src={`${endpoint}/${src}`} {...restProps} />;
}

Image.propTypes = {
  endpoint: PropTypes.string,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
};

Image.defaultProps = {
  endpoint: process.env.BOILERPLATE_PWA_CDN,
  alt: '',
};
export default Image;

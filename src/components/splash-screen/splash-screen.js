import React, { memo } from 'react';
import PropTypes from 'prop-types';
import t from '../../i18n';

function SplashScreen({ showTryAgain, onTryAgain }) {
  console.log(onTryAgain());
  return (
    <div>{showTryAgain && (
        <span>{t('tryAgain')}</span>
    )}</div>
  );
}

SplashScreen.propTypes = {
  showTryAgain: PropTypes.bool,
  onTryAgain: PropTypes.func,
};

SplashScreen.defaultProps = {
  showTryAgain: false,
  onTryAgain: () => {},
};

export default memo(SplashScreen);

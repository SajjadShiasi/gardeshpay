import React from 'react';
import PropTypes from 'prop-types';

function Page({ children, className }) {
  return <div className={className}>{children}</div>;
}

Page.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

Page.defaultProps = {
  children: null,
  className: '',
};

export default Page;

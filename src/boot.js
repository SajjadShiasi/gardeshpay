import React, { useState } from 'react';
import SplashScreen from './components/splash-screen';
import Router from './router';

const ContractProvider = () => {
  const [loading] = useState(false);
  const [error] = useState(false);

  if (loading || error) {
    return <SplashScreen showTryAgain={Boolean(error)} />;
  }
  return (
    <>
      <Router />
    </>
  );
};

function Boot() {
  return <ContractProvider />;
}

export default Boot;

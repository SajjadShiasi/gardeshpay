import {
  createElement,
  Fragment,
  isValidElement as isReactElement,
} from 'react';
import { getItem, setItem } from '../utils/storage';
import fa from './fa';

type Language = 'fa' | 'en';
type Direction = 'rtl' | 'ltr';
type Locale = 'fa-IR' | 'en-GB';

interface LanguageInterface {
  locale: Locale;
  language: Language;
  direction: Direction;
}

const FA: Locale = 'fa-IR';
const EN: Locale = 'en-GB';

const localeConfig: { [key: string]: LanguageInterface } = {
  [FA]: {
    locale: 'fa-IR',
    language: 'fa',
    direction: 'rtl',
  },
  [EN]: {
    locale: 'en-GB',
    language: 'en',
    direction: 'ltr',
  },
};

function getLocaleFromStorage(): Locale {
  return (
    getItem('fa') || process.env.BOILERPLATE_PWA_DEFAULT_LOCALE
  );
}

const currentLocale: Locale = getLocaleFromStorage();

let currentLocaleKeys: { [key: string]: string } = {};

function getCurrentLocale(): LanguageInterface {
  return {
    ...localeConfig[currentLocale],
  };
}

function setLocaleToStorage(locale: Locale): void {
  setItem('fa', locale);
}

function setLocale(locale: Locale): void {
  setLocaleToStorage(locale);
}

function getTranslateWithKey(key: string): string | undefined {
  /**
   * fa[key] is fallback
   */
  return currentLocaleKeys[key] || fa[key];
}

async function setup() {
  switch (currentLocale) {
    case FA: {
      currentLocaleKeys = fa;
      break;
    }
    case EN: {
      currentLocaleKeys = (await import('./en')).default;
      break;
    }
    default: {
      currentLocaleKeys = fa;
    }
  }
}

function translate(key: string, replace?): Array<any> | string {
  if (replace) {
    let valueToReturn = [getTranslateWithKey(key)];

    let includesReactComponent = false;

    const replacer = (array, searchValue, replaceValue) => {
      for (let i = 0; i < array.length; i += 1) {
        if (typeof array[i] === 'string' && array[i].includes(searchValue)) {
          const [before, after] = array[i].split(searchValue);

          array.splice(i, 1, before, replaceValue, after);
          return array;
        }
      }
      return array;
    };

    Object.keys(replace).forEach((placeholder) => {
      const searchValue = `{{${placeholder}}}`;
      const replaceValue = replace[placeholder];

      if (isReactElement(replaceValue)) {
        includesReactComponent = true;
      }

      valueToReturn = replacer(valueToReturn, searchValue, replaceValue);
    });

    if (includesReactComponent) {
      return valueToReturn.map((value, index) =>
        createElement(Fragment, { key: index }, value),
      );
    }

    return valueToReturn.join('');
  }

  return getTranslateWithKey(key);
}

export { getCurrentLocale, setLocale, setup, FA, EN };

export default translate;

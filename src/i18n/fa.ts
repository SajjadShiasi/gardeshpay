export default {
  loginPage: 'ورود',
  back: 'بازگشت',
  yes: 'بلی',
  no: 'خیر',
  close: 'بستن',
  next: 'بعدی',
  prev: 'قبلی',
};

import React from 'react';
import { Provider } from 'react-redux';
import Boot from './boot';
import { FA, getCurrentLocale } from './i18n';
import store from './redux/store/configure-store';
import './App.css';

if (process.env.NODE_ENV === 'development') {
  window.store = store;
}
const fallbackFonts = `
-apple-system,
    BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell',
    'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif
`;

function App() {
  const { locale } = getCurrentLocale();

  return (
    <Provider store={store}>
          <div
            className='App'
            id='app'
            style={{
              fontFamily:
                locale === FA
                  ? `IRANSansMobile-fa, ${fallbackFonts}`
                  : `IRANSansMobile-en, ${fallbackFonts}`,
            }}
          >
            <Boot />
          </div>
    </Provider>
  );
}

export default App;

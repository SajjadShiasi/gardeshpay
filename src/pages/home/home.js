/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Divider, List, Skeleton } from 'antd';
import Page from '../../components/page/page';
import Service from '../../utils/service';


export default function Home() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  async function list() {
    setLoading(true);
    const getList = await Service.sendApi('get', 'List', true);
    try {
      if (getList.status === 200) {
        console.log(getList);
        setData(getList?.data?.users);
        setLoading(false);
      }
    } catch (error) {
      console.log(error, data);
      setLoading(false);
    }
  }
  useEffect(() => {
    list();
  }, []);
  return (
    <Page>
      {' '}
      <div
        id='scrollableDiv'
        style={{
          height: 400,
          overflow: 'auto',
          padding: '0 16px',
          border: '1px solid rgba(140, 140, 140, 0.35)',
        }}
      >
        <InfiniteScroll
          dataLength={data.length}
          next={list}
          hasMore={data.length < 50}
          loader={<Skeleton avatar paragraph={{ rows: 1 }} active />}
          endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
          scrollableTarget='scrollableDiv'
        >
          <List
            dataSource={data}
            renderItem={(item) => (
              <List.Item key={item.id}>
                <List.Item.Meta
                  title={item.username}
                  description={item.email}
                />
                <div>{item.created}</div>
              </List.Item>
            )}
          />
        </InfiniteScroll>
      </div>
    </Page>
  );
}

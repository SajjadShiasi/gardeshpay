import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Col, Form, Input, Row } from 'antd';
import Page from '../../components/page/page';
import { setToken } from '../../service/auth';
import Service from '../../utils/service';
import Pic from './login.png';
import styles from './login.module.css';

export default function Login() {
  const history = useHistory();
  async function login(values) {
    const info = {
      email: values.email,
      password: values.password,
    };

    const response = await Service.sendApi('post', 'Login', info, true);
    try {
      if (response?.status === 200) {
        setToken(response.data?.session?.id);
        history.push('/');
      }
    } catch (error) {
      if (error.response?.status === 500) {
        alert(response?.data?.detail);
      }
    }
  }
  // const onFinish = (values) => {
  //   console.log('Success:', values);
  // };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <Page>
      <Row gutter={[0, 8]}>
        <Col flex={1}>
          <div className={styles.wrapper}>
            {' '}
            <Form
              name='basic'
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={login}
              onFinishFailed={onFinishFailed}
              autoComplete='off'
            >
              <Form.Item
                label='Username'
                name='email'
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label='Password'
                name='password'
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button
                  className={styles.submit}
                  type='primary'
                  htmlType='submit'
                >
                  Submit
                </Button>
              </Form.Item>
              <Button
                className={styles.login}
                onClick={() => history.push('/register')}
              >
                Register?
              </Button>
            </Form>
          </div>
        </Col>
        <Col flex={1}>
          <div className={styles.pic}>
            <img src={Pic} alt='login' />
          </div>{' '}
        </Col>
      </Row>
    </Page>
  );
}

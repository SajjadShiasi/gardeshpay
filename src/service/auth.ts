import {  getItem, removeItem, setItem } from '../utils/storage';
import { storageKeys } from './storage';

export const getToken = () => getItem(storageKeys.token);

export const setToken = (value) => setItem(storageKeys.token, value);

export const removeToken = () => {
  removeItem(storageKeys.token);
};

export const logout = () => {
  console.warn('You are going to be logged out...');
  removeToken();
};

export const hasToken = () => {
  const token = getItem(storageKeys.token);

  return !!token && token !== 'null';
};

/* eslint-disable consistent-return */
/* eslint-disable class-methods-use-this */

import axios from 'axios';
// import { getToken } from '../service/auth';

const apiBaseUrl = 'https://api.m3o.com/v1/user/';

class Service {
  async request(method, url, dataModel, isAuthorized) {
    return this.sendApi(method, url, dataModel, isAuthorized);
  }

  async sendApi(apiMethod, url, dataModel, isAuthorized = true) {
    try {
      let response = {};
      if (apiMethod === 'get') {
        response = await axios({
          method: apiMethod,
          url: `${apiBaseUrl + url}?${Object.keys(dataModel)
            // eslint-disable-next-line sonarjs/no-nested-template-literals
            .map((key) => `${key}=${dataModel[key]}`)
            .join('&')}`,
          headers: this.headerConf(isAuthorized),
        });
      } else {
        response = await axios({
          method: apiMethod,
          url: apiBaseUrl + url,
          data: dataModel,
          headers: this.headerConf(isAuthorized),
        });
      }
      return this.handleSuccess(response);
    } catch (error) {
      return this.handleError(error);
    }
  }

  headerConf(isAuthorized) {
    if (isAuthorized) {
      return {
        Authorization: `Bearer ${'Y2M2NTZiMGEtNmM5ZS00OTUwLTkyNWQtN2ZmZmM5NmJiMjQ0'}`,
      };
    }
    return '';
  }

  handleSuccess(response) {
    return Promise.resolve(response);
  }

  handleError(error) {
    if (error.response.status === '500') {
      alert(error.response?.data?.detail);
    }
    // if (error.response.status === '403') {
    //   //
    // }
    // if (error.response.status === '401') {
    //   //
    // }
    // if (error.response.status === '404') {
    //   //
    // }
    return alert(error.response?.data?.detail);
  }
}

export default new Service();

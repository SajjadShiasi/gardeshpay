const hasLocalStorage = (() => {
  if (!('localStorage' in window)) {
    return false;
  }

  try {
    const key = ' pwa localstorage test available';
    localStorage.setItem(key, 'test');
    localStorage.getItem(key);
    localStorage.removeItem(key);
  } catch (error) {
    return false;
  }

  return true;
})();

const hasSessionStorage = (() => {
  if (!('sessionStorage' in window)) {
    return false;
  }

  try {
    const key = ' pwa sessionStorage test available';
    sessionStorage.setItem(key, 'test');
    sessionStorage.getItem(key);
    sessionStorage.removeItem(key);
  } catch (error) {
    return false;
  }

  return true;
})();

function temporaryStorage() {
  const data = {};

  return {
    getItem: key => data[key] || null,
    setItem: (key, value) => {
      data[key] = String(value);
    },
    removeItem: key => {
      delete data[key];
    },
  };
}

const storage = (type = localStorage) => {
  switch (type) {
    case localStorage:
      return hasLocalStorage ? localStorage : temporaryStorage();
    case sessionStorage:
      return hasSessionStorage ? sessionStorage : temporaryStorage();
    default:
      return temporaryStorage();
  }
};

export const getItem = (key: string, targetStorage?: any): any => storage(targetStorage).getItem(key);

export const getParsedItem = (key: string, targetStorage?: any): any => JSON.parse(storage(targetStorage).getItem(key));

export const setItem = (key: string, value: any, targetStorage?: any) =>
  storage(targetStorage).setItem(key, value);

export const removeItem = (key: string, targetStorage?: any) =>
  storage(targetStorage).removeItem(key);

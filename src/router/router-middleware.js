import { hasToken } from '../service/auth';
import paths from './paths';

const response = (action, data) => ({ action, data });

export function needAuth(route) {
  if (hasToken()) {
    return response('pass', route);
  }
  return response('redirect', paths.login);
}

export function needUnAuth(route) {
  if (!hasToken()) {
    return response('pass', route);
  }
  return response('redirect', paths.home);
}

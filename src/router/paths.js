/**
 * TIP: All paths rule is kebab-case
 */

const paths = {
  login: '/login',
  home: '/',
  register: '/register',
};

export default paths;

import React, { memo, Suspense } from 'react';
import { Redirect, Route, Router, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import SplashScreen from '../components/splash-screen';
import history from '../utils/history';
import routes from './route-configs';

function GuardedRoute({ middleware, ...r }) {
  let finalProps = { ...r };
  for (let i = 0; i < middleware.length; i += 1) {
    const currentMiddleware = middleware[i];
    const response = currentMiddleware(finalProps);
    if (response.action === 'redirect') {
      return <Redirect to={response.data} />;
    }
    if (response.action === 'pass') {
      finalProps = response.data || finalProps;
    }
  }

  const { component, ...rest } = finalProps;
  return <Route {...rest} component={component()} />;
}

GuardedRoute.propTypes = {
  middleware: PropTypes.arrayOf(PropTypes.func),
};

GuardedRoute.defaultProps = {
  middleware: [],
};

const RouterProvider = memo(() => (
  <Router history={history}>
    <Suspense fallback={<SplashScreen />}>
      <Switch>
        {routes.map((route) => (
          <GuardedRoute
            key={route.path}
            exact={route.exact ?? true}
            {...route}
          />
        ))}
      </Switch>
    </Suspense>
  </Router>
));

export default RouterProvider;

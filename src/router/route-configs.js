import { lazy } from 'react';
// import Home from '../pages/home';
// import Login from '../pages/login';
import paths from './paths';
import { needAuth, needUnAuth } from './router-middleware';

const Home = lazy(() => import('../pages/home'));
const Login = lazy(() => import('../pages/login'));
const Register = lazy(() => import('../pages/register'));

export default [
  {
    path: paths.home,
    component: () => Home,
    middleware: [needAuth],
  },
  {
    path: paths.login,
    component: () => Login,
    middleware: [needUnAuth],
  },
  {
    path: paths.register,
    component: () => Register,
    middleware: [needUnAuth],
  },

];

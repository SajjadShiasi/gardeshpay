export const SET_CONFIG = Symbol('Sets config for the entire app');

window.actions = {
  ...window.actions,
  SET_CONFIG,
};

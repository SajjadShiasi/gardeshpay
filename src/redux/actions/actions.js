import { useCallback } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

/**
 * Dispatch function to be called
 * to make use of the memo function if needed
 * @param {string} type
 */

export function useDispatchToProps(type) {
  const dispatch = useDispatch();
  const handleDispatch = (payload) => {
    dispatch({ type, payload });
  };
  return useCallback(handleDispatch, [type, dispatch]);
}

/**
 * MapStateToProps to be called
 * that makes a shallow compare to
 * avoid unecessary rerenders
 * @param {*} selection
 */

export function useStateToProps(selection, equality = shallowEqual) {
  const handleState = useSelector(selection, equality);
  return handleState;
}

export default {};
